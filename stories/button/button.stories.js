import { createButton } from './button';
import ButtonDocumentation from './button.md';

export default {
  title: 'Design System/Atoms/Button',
  decorators: [(story) => 
    {
      const decorator = document.createElement('div')
      decorator.style.margin = '24px'
      decorator.appendChild(story())
      return decorator
    },
  ],
  parameters: {
    docs: {
      page: null,
      description: {
        component: ButtonDocumentation
      }
    },
    actions: {
      handles: ['mouseover'],
    },
    backgrounds: {
      default: 'default',
      values: [
        {
          name: 'blackfiday',
          value: '#000000',
        },
        {
          name: 'default',
          value: '#ffffff',
        }
      ]
    }
  },
  argTypes: {
    label: {
      name: "Label",
      control: {
        type: "text",
      }
    },
    style: {
      name: 'Style',
      options: ['outline', 'filled'],
      control: {
        type: 'radio',
      }
    },
    size: {
      name: 'Size',
      options: ['small', 'medium', 'large'],
      control: {
        type: 'select',
      }
    },
    onClick: {
      description: 'The function to call when the button is clicked',
      action: 'clicked',
    }
  }
}

const Template = ({label, ...args}) => {
  return createButton({label, ...args});
}

export const Default = Template.bind({});
Default.args = {
  label: 'Button',
}