import { createCard } from './card.js';

export default {
  title: 'Design System/Atoms/Card',
}

const Template = ({title, description, ...args}) => {
  return createCard({title, description, ...args});
}

export const Default = Template.bind({});
Default.args = {
  title: 'Title',
  description: 'Description',
}